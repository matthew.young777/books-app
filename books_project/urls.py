"""books_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, reverse_lazy
from django.views.generic.base import RedirectView
from django.views.generic.base import TemplateView #new
from django.contrib.auth import views as auth_views #not used rn

urlpatterns = [
    path('admin/', admin.site.urls),
    path('books/', include("books.urls")),
    path('magazines/', include("books.magazines_urls")),
    path("accounts/", include("django.contrib.auth.urls")),
    path('', TemplateView.as_view(template_name='home.html'), name='home'),
    #path('accounts/login/', auth_views.LoginView.as_view(), name ='login'), #added for login info, name = 'login'
    #path('accounts/logout/', auth_views.LogoutView.as_view(), name='logout'),
]
