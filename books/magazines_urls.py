from django.urls import path, reverse_lazy
from django.views.generic.base import RedirectView
from books.views import (
    delete_magazine,
    list_magazines,
    show_magazine,
    create_magazine,
    update_magazine,
    delete_magazine,
    add_issue,
    show_genre,
)

urlpatterns = [
    
    # path(
    #     "",
    #     RedirectView.as_view(url=reverse_lazy("magazines_list")),
    #     name="magazine_home",
    # ),
    path("list/", list_magazines, name="magazines_list"),
    path("<int:pk>/", show_magazine, name="detail_magazine"),
    path("create/", create_magazine, name="create_magazine"),
    path("<int:pk>/edit/", update_magazine, name="edit_magazine"),
    path("<int:pk>/delete/", delete_magazine, name="delete_magazine"),
    path("", list_magazines, name="magazines_list"),
    path("<int:pk>/add/", add_issue, name="add_issue"),
    path("genres/<int:pk>/", show_genre, name="detail_genre")

]