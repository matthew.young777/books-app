from django.db import models

# Create your models here.



class Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name

class Book(models.Model):
    name = models.CharField(max_length=125, unique=True)
    authors = models.ManyToManyField(Author, related_name="books")
    pages = models.IntegerField(null=True)
    isbn = models.BigIntegerField(null=True)
    cover = models.URLField(null=True, blank=True)
    is_in_print = models.BooleanField()
    pub_date = models.DateField()

    def __str__(self):
        return self.name + " by " + str(self.authors.first())

class BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)


class Genre(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name
class Magazine(models.Model):
    class ReleaseCycle(models.TextChoices):
        WEEKLY = 'WK', ('Weekly')
        BIWEEKLY = "BW", ('Bi-Weekly')
        MONTHLY = "MT", ('Monthly')
        YEARLY = "YR", ('Annually')


    release_cycle = models.CharField(max_length = 2, choices=ReleaseCycle.choices)
    title = models.CharField(max_length=125, unique=True)
    # release_cycle = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    cover = models.URLField(null=True, blank=True)
    genre = models.ManyToManyField("Genre", related_name="magazines")

    def __str__(self):
        return self.title

class Issue(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    cover_img = models.URLField(blank=True)
    date_published = models.DateField(blank=True, null=True)
    issue_number = models.IntegerField()
    page_count = models.IntegerField()
    magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.title