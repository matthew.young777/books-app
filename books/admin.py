from django.contrib import admin
from .models import Book, Magazine, Author, BookReview, Genre, Issue

class BookAdmin(admin.ModelAdmin):
    pass

admin.site.register(Book, BookAdmin)

class MagazineAdmin(admin.ModelAdmin):
    pass

admin.site.register(Magazine, MagazineAdmin)

class BookReviewAdmin(admin.ModelAdmin):
    pass

admin.site.register(BookReview, BookReviewAdmin)

class AuthorAdmin(admin.ModelAdmin):
    pass

admin.site.register(Author,AuthorAdmin)

class GenreAdmin(admin.ModelAdmin):
    pass

admin.site.register(Genre, GenreAdmin)

class IssueAdmin(admin.ModelAdmin):
    pass

admin.site.register(Issue, IssueAdmin)