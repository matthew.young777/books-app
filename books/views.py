from django.shortcuts import render, redirect, HttpResponseRedirect, get_object_or_404

from books.forms import BooksForm, MagazinesForm, IssuesForm
from books.models import Book, Magazine, Genre, Author, BookReview, Issue
    

def show_books(request):
    context = {
        "books": Book.objects.all() if Book else [],
    }

    return render(request, "books/list.html", context)

def create_book(request):
    context = {}

    form = BooksForm(request.POST or None)
    if form.is_valid():
        book = form.save()
        return redirect("detail_book", pk=book.pk)
    context['form'] = form
    return render(request, "books/create.html", context)

def show_book(request, pk):
    context = {
        "book": Book.objects.get(pk=pk) if Book else [],
    }
    return render(request, "books/detail.html", context)

def update_book(request, pk):
    
    book = get_object_or_404(Book, pk = pk)
    form = BooksForm(request.POST or None, instance = book)
    if form.is_valid():
        form.save()
        return redirect("detail_book", pk=pk)
    context ={}
    context['form']= form
    return render(request, "books/update.html", context)

def delete_book(request, pk):
    context ={}
 
    # fetch the object related to passed id
    obj = get_object_or_404(Book, pk = pk)
 
    if request.method =="POST":
        # delete object
        obj.delete()
        # after deleting redirect to
        # home page
        return redirect("books_list")
 
    return render(request, "books/delete.html", context)

def list_magazines(request):
    context = {
        "magazines": Magazine.objects.all() if Magazine else [],
    }

    return render(request, "magazines/list.html", context)

def show_magazine(request, pk):
    context = {
        "magazine": Magazine.objects.get(pk=pk) if Magazine else [],
    }
    return render(request, "magazines/detail.html", context)

def create_magazine(request):
    context = {}

    form = MagazinesForm(request.POST or None)
    if form.is_valid():
        mag = form.save()
        return redirect("detail_magazine", pk=mag.pk)
    context['form'] = form
    return render(request, "magazines/create.html", context)

def update_magazine(request, pk):
    
    magazine = get_object_or_404(Magazine, pk = pk)
    form = MagazinesForm(request.POST or None, instance = magazine)
    if form.is_valid():
        form.save()
        return redirect("detail_magazine", pk=pk)
    context ={}
    context['form']= form
    return render(request, "magazines/update.html", context)

def delete_magazine(request, pk):
    context ={}
 
    # fetch the object related to passed id
    obj = get_object_or_404(Magazine, pk = pk)
 
    if request.method =="POST":
        # delete object
        obj.delete()
        # after deleting redirect to
        # home page
        return redirect("magazines_list")
 
    return render(request, "magazines/delete.html", context)

def add_issue(request, pk):
    context = {}
    # magazine = get_object_or_404(Magazine, pk = pk)
    form = IssuesForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("detail_magazine", pk=pk)
    context['form'] = form
    return render(request, "issues/create.html", context)

def show_genre(request, pk):
    context = {
        "genre": Genre.objects.get(pk=pk) if Genre else [],
    }
    return render(request, "genres/detail.html", context)