from django.urls import path, reverse_lazy
from django.views.generic.base import RedirectView
from books.views import (
    delete_book,
    show_books,
    create_book,
    show_book,
    update_book,
    delete_book
)

urlpatterns = [
    path("list/", show_books, name="books_list"),
    path("create/", create_book, name="create_book"),
    path("<int:pk>/", show_book, name="detail_book"),
    path("<int:pk>/edit/", update_book, name="update_book"),
    path("<int:pk>/delete/", delete_book, name="delete_book"),
    path("", show_books, name="books_list"),
]
