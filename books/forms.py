from django import forms
from .models import Book, Magazine, Genre, Author, BookReview, Issue
from django.forms.widgets import CheckboxSelectMultiple

class DateInput(forms.DateInput):
    input_type = 'date'

class BooksForm(forms.ModelForm):

    class Meta:
        model = Book

        fields = [
            "name",
            "authors",
            "pages",
            "isbn",
            "cover",
            "is_in_print",
            "pub_date"
        ]

        widgets = { 
            'pub_date': DateInput(),
        } 

    def __init__(self, *args, **kwargs):
    
        super(BooksForm, self).__init__(*args, **kwargs)
    
        self.fields["authors"].widget = CheckboxSelectMultiple()
        self.fields["authors"].queryset = Author.objects.all()

class MagazinesForm(forms.ModelForm):
    class Meta:
        model = Magazine
        fields = [
            "title",
            "release_cycle",
            "description",
            "cover",
            "genre",
        ]

    def __init__(self, *args, **kwargs):
        
        super(MagazinesForm, self).__init__(*args, **kwargs)
        
        self.fields["genre"].widget = CheckboxSelectMultiple()
        self.fields["genre"].queryset = Genre.objects.all()
        


class IssuesForm(forms.ModelForm):
    class Meta:
        model = Issue
        
        fields = [
            "title",
            "description",
            "cover_img",
            "date_published",
            "issue_number",
            "page_count",
            "magazine"
        ]

        widgets = { 
            'date_published': DateInput(),
        } 